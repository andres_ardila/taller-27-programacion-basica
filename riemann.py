import math

def riemann(a, b, n):
    sumat=0
    for i in range(n):
        particion=(b-a)/n
        x=a+(i*particion)
        funcion=x**3
        suma=funcion*particion
        sumat=sumat+suma

    return sumat

print("El área bajo la curva es:", riemann(1, 2, 20)) 